package com.gitlab.timenegative.old38.engine

import com.gitlab.timenegative.old38.engine.core.Scene

class Game {
    protected var scenes : List<Scene>? = null

    fun start() {
        scenes = ArrayList<Scene>();
    }

    @Throws(IllegalStateException::class)
    fun update() {
        try {
            for (scene: Scene in scenes!!) {

            }
        } catch (e : NullPointerException) {
            e.printStackTrace()
            throw IllegalStateException("Can't update game state", e)
        }
    }

    fun stop() {
        scenes = null;
    }
}